/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package consistencycheckers;
import fileexecutors.UnixFileExecutors;
import objects.*;
import readers.AxiomsReaders;
import writers.AxiomsWriters;

/**
 *
 * @author pawelgarbacz
 */
public class ConsistencyCheckers 
{
    public ConsistencyTypes check(
        AxiomSetContainers axiomSetContainer,
        AxiomsReaders axiomsReader,
        AxiomsWriters axiomsWriter,
        UnixFileExecutors unixFileExecutor)
    {
        String proverInputFileName = 
            axiomsWriter.write(
                axiomSetContainer);
            
        unixFileExecutor.writeShellFile(
            proverInputFileName);
            
        String proverOutputFileName = 
            unixFileExecutor.execCmd();
            
        String output = 
            axiomsReader.readAxiomTextFromTextFile(
                proverOutputFileName);
            
        ConsistencyTypes consistancyStatus = 
            axiomsReader.getConsistencyStatus(
                output);
        
        return 
            consistancyStatus;
    }
}

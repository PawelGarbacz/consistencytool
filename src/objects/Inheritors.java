/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objects;

import java.util.HashMap;
import java.util.TreeMap;

/**
 *
 * @author pawelgarbacz
 */
public class Inheritors 
{
    private TreeMap<Integer, AxiomSetContainers> _axiomSetPowersetCodedMap;
    private HashMap<Axioms, Integer> _axiomCodingMap;
    private CodingAxiomSetContainers _initialAxiomSetContainter;
    
    public Inheritors(
        TreeMap<Integer, AxiomSetContainers> axiomSetPowersetCodedMap, 
        CodingAxiomSetContainers initialAxiomSetContainter)
    {
        _axiomSetPowersetCodedMap = 
            axiomSetPowersetCodedMap;
        
        _initialAxiomSetContainter = 
            initialAxiomSetContainter;
        
        _axiomCodingMap = 
            _initialAxiomSetContainter.axiomCodingMap;
    }
    
    public void inherit(
        AxiomSetContainers axiomSetContainter)
    {
        for (Axioms axiom : axiomSetContainter.AxiomsSet)
        {
            Integer axiomCode = 
                _axiomCodingMap.get(
                    axiom);
            
            Integer subContainerCode = 
                axiomSetContainter.code - (int) Math.pow(2, axiomCode);
            
            AxiomSetContainers axiomSetSubContainter = 
                _axiomSetPowersetCodedMap.get(
                    subContainerCode);
                
            axiomSetContainter.SubSetContainers.add(
                axiomSetSubContainter);
            
            axiomSetSubContainter.SuperSetContainers.add(
                axiomSetContainter);
            
            for (AxiomSetContainers axiomSetCurrentSuperContainter : axiomSetContainter.SuperSetContainers)
            {
                axiomSetCurrentSuperContainter.SubSetContainers.add(
                    axiomSetSubContainter);
                
                axiomSetSubContainter.SuperSetContainers.add(
                    axiomSetCurrentSuperContainter);
            }
            
            inherit(
                axiomSetSubContainter);
        }      
    }
    
    public void inherit()
    {
        for (AxiomSetContainers axiomSetContainer : _axiomSetPowersetCodedMap.values())
        {
            Integer axiomSetContainerCode = 
                axiomSetContainer.code;
            
            String axiomSetContainerBinaryCode = 
                Integer.toBinaryString(
                    axiomSetContainerCode);
            
            Integer codeLength = 
                axiomSetContainerBinaryCode.length();
            
            for (int i=codeLength-1; i>=0;i--)
            {
                if (axiomSetContainerBinaryCode.substring(i, i+1).equals("1"))
                {
                    Integer axiomSetSubContainerCode = 
                        axiomSetContainerCode - (int) Math.pow(2, codeLength-i-1);
                
                    AxiomSetContainers axiomSetSubContainer = 
                        _axiomSetPowersetCodedMap.get(
                            axiomSetSubContainerCode);
                    
                    axiomSetContainer.SubSetContainers.add(
                        axiomSetSubContainer);
                    
                    axiomSetSubContainer.SuperSetContainers.add(
                        axiomSetContainer);
                    
                    for (AxiomSetContainers axiomSetSubSubContainter : axiomSetSubContainer.SubSetContainers)
                    {
                        axiomSetContainer.SubSetContainers.add(
                            axiomSetSubSubContainter);
                        
                        axiomSetSubSubContainter.SuperSetContainers.add(
                            axiomSetContainer);
                    }
                    
                    for (AxiomSetContainers axiomSetSuperContainter : axiomSetContainer.SuperSetContainers)
                    {
                        axiomSetSubContainer.SuperSetContainers.add(
                            axiomSetSuperContainter);
                        
                        axiomSetSuperContainter.SubSetContainers.add(
                            axiomSetSubContainer);
                    }
                }
                
            }
            
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objects;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;

/**
 *
 * @author pawelgarbacz
 */
public class Powersets 
{
    public Set<AxiomSetContainers> AxiomSetContainersSet = 
        new HashSet<>();
    
    public TreeMap<Integer, AxiomSetContainers> AxiomCodingMap = new TreeMap<>();
    
    private CodingAxiomSetContainers _codingAxiomSetContainer;
    
    
    public Powersets(
        CodingAxiomSetContainers codingAxiomSetContainer)
    {
        _codingAxiomSetContainer = 
            codingAxiomSetContainer;
    }
    
    private HashMap<Set<Axioms>, AxiomSetContainers> map = new HashMap<>();
    
    public Set<Set<Axioms>> getPowerSet(
        Set<Axioms> originalSet) 
    {
        Set<Set<Axioms>> sets = 
            new HashSet<>();
    
        if (originalSet.isEmpty()) 
        {
            AxiomSetContainers axiomSetContainer = 
                new AxiomSetContainers();
            
            Set<Axioms> set = 
                new HashSet<>();
            
            axiomSetContainer.AxiomsSet = set;
            
            axiomSetContainer.code = 0;
            
            AxiomSetContainersSet.add(
                axiomSetContainer);
            
            AxiomCodingMap.put(
                axiomSetContainer.code, 
                axiomSetContainer);
            
            sets.add(
                set);
            
            map.put(
                set, 
                axiomSetContainer);
            
            return 
                sets;
        }
    
//        AxiomSetContainers axiomSetSuperSuperContainer = 
//            map.get(
//                originalSet);
//        
        List<Axioms> list = 
            new ArrayList<>(
                originalSet);
    
        Axioms head = 
            list.get(0);
    
        Set<Axioms> rest = 
            new HashSet<>(
                list.subList(1, list.size())); 
    
        Set<Set<Axioms>> localPowerset = getPowerSet(rest);
        
        for (Set<Axioms> set : localPowerset) 
        {
            Set<Axioms> newSet = 
                new HashSet<>();
            
            newSet.add(
                head);
            
            newSet.addAll(
                set);
            
            sets.add(
                newSet);
            
            sets.add(
                set);
            
            AxiomSetContainers axiomSetSubContainer = 
                map.get(
                    set);
            
            AxiomSetContainers axiomSetSuperContainer = 
                new AxiomSetContainers();
            
            axiomSetSuperContainer.AxiomsSet = 
                newSet;
            
            Integer headCoding = 
                _codingAxiomSetContainer.axiomCodingMap.get(
                    head);
            
            axiomSetSuperContainer.code = 
                axiomSetSubContainer.code + (int) Math.pow(2, headCoding);
            
//            axiomSetSubContainer.SuperContainers.add(
//                axiomSetSuperContainer);
//            
//            axiomSetSuperContainer.SubContainers.add(
//                axiomSetSubContainer);
            
            AxiomSetContainersSet.add(
                axiomSetSuperContainer);
            
            map.put(
                newSet, 
                axiomSetSuperContainer);
            
              AxiomCodingMap.put(
                axiomSetSuperContainer.code, 
                axiomSetSuperContainer);        
        }	
        
        return 
            sets;
    }
}

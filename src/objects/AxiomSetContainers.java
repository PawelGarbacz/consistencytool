/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objects;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

/**
 *
 * @author pawelgarbacz
 */
public class AxiomSetContainers
{
    public Set<Axioms> AxiomsSet = new HashSet<>();
    public HashMap<String, Axioms> AxiomLabelsMap = new HashMap<>();
    public ConsistencyTypes ConsistencyType = ConsistencyTypes.untested;
    public Integer code;
    public HashSet<AxiomSetContainers> SubSetContainers = new HashSet<>();
    public HashSet<AxiomSetContainers> SuperSetContainers = new HashSet<>();
    
    @Override
    public String toString()
    {
        if (AxiomsSet.isEmpty())
            return "empty";
        
        TreeSet<String> axiomLabelsOrderSet = 
            new TreeSet<>();
        
        for (Axioms axiom : AxiomsSet)
            axiomLabelsOrderSet.add(
                axiom.Label);
        
        String axiomSetConcatenation = "{";
        
        for (String axiomLabel : axiomLabelsOrderSet)
            axiomSetConcatenation = 
                axiomSetConcatenation + axiomLabel + " ";
        
        axiomSetConcatenation = 
            axiomSetConcatenation.substring(
                0, 
                axiomSetConcatenation.lastIndexOf(" "));
        
        axiomSetConcatenation = 
            axiomSetConcatenation +"}";
        
        return 
            axiomSetConcatenation;
    }
    
    public void setConsistentType()
    {
        ConsistencyType = 
            ConsistencyTypes.consistent;
        
        for (AxiomSetContainers axiomSetSubContainer : SubSetContainers)
            axiomSetSubContainer.ConsistencyType  = 
                ConsistencyTypes.consistent;
    }
    
    public void setInconsistentType()
    {
        ConsistencyType = 
            ConsistencyTypes.inconsistent;
        
        for (AxiomSetContainers axiomSetSuperContainer : SuperSetContainers)
            axiomSetSuperContainer.ConsistencyType  = 
                ConsistencyTypes.inconsistent;
    }
}

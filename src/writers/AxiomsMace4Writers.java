/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package writers;

/**
 *
 * @author pawelgarbacz
 */
public class AxiomsMace4Writers extends AxiomsProver9Mace4CommonWriters
{
    private int domainSize=2;
    
    public AxiomsMace4Writers(
        String constantAxiomSetText)
    {
        super(
            constantAxiomSetText);
        
        _toolName = 
            "mace4_";
    }
    
    @Override
    protected void prepareSpecificContent()
    {
        String maceSpecificContent = "";
        
        maceSpecificContent = 
            "assign(start_size," + domainSize + ")." + System.lineSeparator();
        
        maceSpecificContent = 
            maceSpecificContent + "set(prolog_style_variables)." + System.lineSeparator();
        
        maceSpecificContent = 
            maceSpecificContent + "assign(max_seconds, 60)." + System.lineSeparator();
        
        maceSpecificContent = 
            maceSpecificContent + "assign(max_megs, 6000)." + System.lineSeparator();
        
        _axiomsConcatenation = 
            maceSpecificContent + _axiomsConcatenation;
        
        _axiomsConcatenation = 
            _axiomsConcatenation + 
            System.lineSeparator() +
            "end_of_list.";
    }
}

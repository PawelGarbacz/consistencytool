/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package writers;

import objects.AxiomSetContainers;
import objects.Axioms;

/**
 *
 * @author pawelgarbacz
 */
public class AxiomsEWriters extends AxiomsWriters
{
    public AxiomsEWriters(
        String constantAxiomSetText)
    {
        super(
            constantAxiomSetText);
        
        _toolName = 
            "E";
    }
    
    public String write(
        AxiomSetContainers AxiomSetContainter)
    {
        _axiomSetContainter = 
             AxiomSetContainter;
        
        prepareCommonContent();
        
        write();
        
        return 
            _fileName;
    }
    
    @Override
    protected void concatenate()
    {
        _fileName = "";
        
        for (Axioms axiom : _axiomSetContainter.AxiomsSet)
        {
            _axiomsConcatenation = 
                _axiomsConcatenation + axiom.EContent + System.lineSeparator();
            
            _fileName = 
                _fileName + axiom.Label;
        }
    }

    @Override
    protected String getProperAxiomSetText() 
    {
        String properAxiomSetText = 
            _constantAxiomSetText.substring(
                _constantAxiomSetText.indexOf("TPTP") + 4, 
                _constantAxiomSetText.lastIndexOf("TPTP"));
        
        return 
            properAxiomSetText;
    }
}

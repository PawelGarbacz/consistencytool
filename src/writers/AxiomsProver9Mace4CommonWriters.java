/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package writers;

import objects.AxiomSetContainers;
import objects.Axioms;

/**
 *
 * @author pawelgarbacz
 */
public abstract class AxiomsProver9Mace4CommonWriters extends AxiomsWriters
{

    public AxiomsProver9Mace4CommonWriters(
        String constantAxiomSetText) 
    {
        super(
            constantAxiomSetText);
    }
    
    public String write(
        AxiomSetContainers AxiomSetContainter)
    {
        _axiomSetContainter = 
             AxiomSetContainter;
        
        prepareCommonContent();
        
        prepareSpecificContent();
        
        write();
        
        return 
            _fileName;
    }
    
    protected abstract void prepareSpecificContent();
    
    @Override
    protected void concatenate()
    {
        _fileName = "";
        
        _axiomsConcatenation = 
            "formulas(assumptions)." + 
            System.lineSeparator();
        
        for (Axioms axiom : _axiomSetContainter.AxiomsSet)
        {
            _axiomsConcatenation = 
                _axiomsConcatenation + 
                axiom.Prover9Mace4CommonContent + 
                System.lineSeparator();
            
            _fileName = 
                _fileName + axiom.Label;
        }
    }
    
    protected String getProperAxiomSetText() 
    {
        String properAxiomSetText = 
            _constantAxiomSetText.substring(
                _constantAxiomSetText.indexOf("PROVER9") + 7, 
                _constantAxiomSetText.lastIndexOf("PROVER9"));
        
        return 
            properAxiomSetText;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package writers;

import configurations.ConsistencyConfiguration;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import objects.AxiomSetContainers;

/**
 *
 * @author pawelgarbacz
 */
public abstract class AxiomsWriters 
{
    protected String _fileName;
    protected String _axiomsConcatenation = "";
    protected AxiomSetContainers _axiomSetContainter;
    protected String _toolName;
    protected String _constantAxiomSetText;
    
    public AxiomsWriters(
        String constantAxiomSetText)
    {
        _constantAxiomSetText = 
            constantAxiomSetText;
    }
    
    public abstract String write(
        AxiomSetContainers AxiomSetContainter);
    
    protected void prepareCommonContent()
    {
        concatenate();
        
        addConstantAxioms();
    }
    
    protected void addConstantAxioms()
    {
        String properConstantAxiomSetText = 
            getProperAxiomSetText();
        
        _axiomsConcatenation = 
            _axiomsConcatenation + 
            System.lineSeparator() + 
            properConstantAxiomSetText;
    }
    
    protected abstract String getProperAxiomSetText();
    
    protected void write()
    {
        _fileName = 
            ConsistencyConfiguration.MainFolder + _toolName + _fileName + ".in";
        
        try (Writer writer = new BufferedWriter(new OutputStreamWriter(
              new FileOutputStream(_fileName), "utf-8"))) 
        {
            writer.write(
                _axiomsConcatenation);
        } 
        catch (IOException ex) {ex.printStackTrace();}
    }
    
    protected abstract void concatenate();
}

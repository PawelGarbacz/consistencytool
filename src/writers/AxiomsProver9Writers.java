/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package writers;

/**
 *
 * @author pawelgarbacz
 */
public class AxiomsProver9Writers extends AxiomsProver9Mace4CommonWriters
{
    public AxiomsProver9Writers(
        String constantAxiomSetText)
    {
        super(
            constantAxiomSetText);
        
        _toolName = 
            "prover9";
    }
    
    @Override
    protected void prepareSpecificContent()
    {
        String proverSpecificContent = "";
        
        proverSpecificContent = 
            proverSpecificContent + "set(prolog_style_variables)." + System.lineSeparator();
        
        proverSpecificContent = 
            proverSpecificContent + "set(auto2)." + System.lineSeparator();
        
        //proverSpecificContent = 
        //    proverSpecificContent + "assign(max_seconds, 60)." + System.lineSeparator();
        
        proverSpecificContent = 
            proverSpecificContent + "assign(sos_limit, 10000)." + System.lineSeparator();
        
        _axiomsConcatenation = 
            proverSpecificContent + _axiomsConcatenation;
        
        _axiomsConcatenation = 
            _axiomsConcatenation + 
            System.lineSeparator() +
            "end_of_list." +
            System.lineSeparator() +
            "formulas(goals)." +
            System.lineSeparator() +
            "end_of_list.";
    }
}

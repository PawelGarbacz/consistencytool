/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package readers;

import java.util.Set;
import java.util.TreeMap;
import objects.AxiomSetContainers;
import objects.Axioms;
import objects.CodingAxiomSetContainers;
import objects.ConsistencyTypes;
import objects.Powersets;

/**
 *
 * @author pawelgarbacz
 */
public abstract class AxiomsReaders 
{
    protected AxiomSetContainers _initialAxiomSetContainter;
    
    public TreeMap<Integer, AxiomSetContainers> getAxiomSetContainersPowerSetCodedMap(
        CodingAxiomSetContainers axiomSetContainter)
    {
        Powersets powerset = 
            new Powersets(axiomSetContainter);
        
        Set<Set<Axioms>> axiomSetPowerset = 
            powerset.getPowerSet(
                axiomSetContainter.AxiomsSet);
        
        TreeMap<Integer, AxiomSetContainers> axiomSetContainerPowersetCodedMap = 
            powerset.AxiomCodingMap;
        
        return 
            axiomSetContainerPowersetCodedMap;
    }
    
    public CodingAxiomSetContainers getFullAxiomSet(
        String fileName)
    {
        CodingAxiomSetContainers axiomSetContainter = 
            readAxiomSetFromTextFile(
                fileName);
        
        return 
            axiomSetContainter;
    }
    
    public String readAxiomTextFromTextFile(
        String fileName) 
    {
        PlainTextReaders plainTextReader = 
            new PlainTextReaders();
        
        String axiomText = 
            plainTextReader.read(
                fileName); 
        
        return 
            axiomText;
    }
    
    public abstract ConsistencyTypes getConsistencyStatus(
        String axiomsConcatenation);
    
    protected abstract CodingAxiomSetContainers readAxiomSetFromTextFile(
        String fileName);
    
    protected abstract String getLabel(
        String axiom);
}

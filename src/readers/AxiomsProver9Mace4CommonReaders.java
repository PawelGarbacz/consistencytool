/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package readers;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import objects.Axioms;
import objects.CodingAxiomSetContainers;

/**
 *
 * @author pawelgarbacz
 */
public abstract class AxiomsProver9Mace4CommonReaders extends AxiomsReaders
{
    @Override
    protected CodingAxiomSetContainers readAxiomSetFromTextFile(
        String fileName) 
    {
        CodingAxiomSetContainers axiomSetContainer = 
            new CodingAxiomSetContainers();
        
        boolean ignoreLine = true;
        
        int codingSeq = 0;
        
        try
        {
            Scanner scan = 
                new Scanner(
                    new File(fileName));
            
            
            while(scan.hasNextLine())
            {
                String line = 
                    scan.nextLine();
                
                if (ignoreLine)
                {
                    if (line.contains("formulas(assumptions)."))
                    {
                        ignoreLine = false;
                        
                        continue;
                    }
                    
                    else
                        continue;
                }
                
                if (line.length()<2)
                    continue;
                
                if (line.contains("end_of_list."))
                    break;
                
                String axiomLabel = 
                    getLabel(
                        line);
                
                Axioms axiom = 
                    new Axioms();

                axiom.Label = 
                    axiomLabel;
                
                axiom.Prover9Mace4CommonContent = 
                    line;

                axiomSetContainer.AxiomLabelsMap.put(
                    axiomLabel, 
                    axiom);

                axiomSetContainer.AxiomsSet.add(
                    axiom);

                axiomSetContainer.axiomCodingMap.put(
                    axiom, 
                    codingSeq);

                codingSeq++;
            }       
        } 
        catch (FileNotFoundException ex) {ex.printStackTrace();}
        
        axiomSetContainer.code = 
            (int) Math.pow(2, codingSeq) - 1;
        
        return 
            axiomSetContainer;
    }
    
    @Override
    protected String getLabel(
        String axiom)
    {
        String label = "";
        
        Pattern labelPattern = 
            Pattern.compile("label\\(([a-zA-Z0-9]+)\\)");
        
        Matcher labelMatch =  
            labelPattern.matcher(axiom);
        
        while (labelMatch.find())
        {
            label = 
                labelMatch.group(1);
        }
        
        return 
            label;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package readers;

import objects.ConsistencyTypes;

/**
 *
 * @author pawelgarbacz
 */
public class AxiomsMace4Readers extends AxiomsProver9Mace4CommonReaders
{
    public ConsistencyTypes getConsistencyStatus(
        String axiomsConcatenation)
    {
        if (axiomsConcatenation.contains(
            "Exiting with 1 model."))
                return ConsistencyTypes.consistent;
        
        return 
            ConsistencyTypes.unknown;
    }
}

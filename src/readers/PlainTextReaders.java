/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package readers;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 *
 * @author pawelgarbacz
 */
public class PlainTextReaders 
{
    public String read(
        String fileName)
    {
        String text = ""; 
        
        try
        {
            Scanner scan = 
                new Scanner(
                    new File(
                        fileName));
            
            while(scan.hasNextLine())
            {
                String line = 
                    scan.nextLine();
                
                text = 
                    text + line + System.lineSeparator();
            }       
        } 
        catch (FileNotFoundException ex) {System.out.println(fileName);}
        
        return 
            text;
    }
}

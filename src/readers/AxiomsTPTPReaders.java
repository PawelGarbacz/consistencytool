/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package readers;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import objects.Axioms;
import objects.CodingAxiomSetContainers;
import objects.ConsistencyTypes;

/**
 *
 * @author pawelgarbacz
 */
public class AxiomsTPTPReaders extends AxiomsReaders
{
    @Override
    public ConsistencyTypes getConsistencyStatus(
        String axiomsConcatenation)
    {
        if (axiomsConcatenation.contains(" Unsatisfiable"))
            return ConsistencyTypes.inconsistent;
        
        if (axiomsConcatenation.contains(" Satisfiable"))
            return ConsistencyTypes.consistent;
        
        return 
            ConsistencyTypes.unknown;
    }
    
    @Override
    protected CodingAxiomSetContainers readAxiomSetFromTextFile(
        String fileName) 
    {
        CodingAxiomSetContainers axiomSetContainer = 
            new CodingAxiomSetContainers();
        
        boolean ignoreLine = true;
        
        int codingSeq = 0;
        
        try
        {
            Scanner scan = 
                new Scanner(
                    new File(
                        fileName));
            
            while(scan.hasNextLine())
            {
                String line = 
                    scan.nextLine();
                
                String axiomLabel = 
                    getLabel(
                        line);
                
                Axioms axiom = 
                    axiomSetContainer.AxiomLabelsMap.get(
                        axiomLabel);
                
                if (axiom == null)
                {
                    axiom = 
                        new Axioms();
                    
                    axiom.Label = 
                        axiomLabel;
                    
                    axiomSetContainer.AxiomLabelsMap.put(
                        axiomLabel, 
                        axiom);
                    
                    axiomSetContainer.AxiomsSet.add(
                        axiom);
                    
                    axiomSetContainer.axiomCodingMap.put(
                        axiom, 
                        codingSeq);
                
                    codingSeq++;
                }
                
                axiom.EContent = 
                    line;
            }       
        } 
        catch (FileNotFoundException ex) {ex.printStackTrace();}
        
        axiomSetContainer.code = 
            (int) Math.pow(2, codingSeq);
        
        return 
            axiomSetContainer;
    }
    
    @Override
    protected String getLabel(
        String axiom)
    {
        String label = "";
        
        Pattern labelPattern = 
            Pattern.compile("fof\\(([a-zA-Z0-9]+)");
        
        Matcher labelMatch =  
            labelPattern.matcher(
                axiom);
        
        while (labelMatch.find())
        {
            label = 
                labelMatch.group(1);
        }
        
        return 
            label;
    }
}

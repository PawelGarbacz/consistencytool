/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package orchestrators;

import configurations.ConsistencyConfiguration;
import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.HashSet;
import java.util.TreeMap;
import objects.AxiomSetContainers;
import objects.Axioms;
import objects.CodingAxiomSetContainers;
import objects.ConsistencyTypes;

/**
 *
 * @author pawelgarbacz
 */
public abstract class ConsistencyWalkers {
    protected TreeMap<Integer, AxiomSetContainers> _axiomSetPowersetCodedMap;
    protected HashMap<Axioms, Integer> _axiomCodingMap;
    protected CodingAxiomSetContainers _initialAxiomSetContainter;
    protected HashSet<AxiomSetContainers> _consistentAxiomSetContaintersSet = new HashSet<>();
    protected HashSet<AxiomSetContainers> _inconsistentAxiomSetContaintersSet = new HashSet<>();
    protected String _constantAxiomSetText;
    protected Writer _writer;
    protected String ToolForConsistencyConfiguration;
    protected String ToolForInconsistencyConfiguration;

    public void setUp(
        TreeMap<Integer, AxiomSetContainers> axiomSetPowersetCodedMap, 
        CodingAxiomSetContainers initialAxiomSetContainter,
        String constantAxiomSetText,
        Writer writer)
    {
        _axiomSetPowersetCodedMap = 
            axiomSetPowersetCodedMap;
        
        _initialAxiomSetContainter = 
            initialAxiomSetContainter;
        
        _axiomCodingMap = 
            _initialAxiomSetContainter.axiomCodingMap;
        
        _constantAxiomSetText = 
            constantAxiomSetText;
        
        _writer = 
            writer;
        
        ToolForConsistencyConfiguration = 
            ConsistencyConfiguration.ToolForConsistencyConfiguration;
        
        ToolForInconsistencyConfiguration = 
            ConsistencyConfiguration.ToolForInconsistencyConfiguration;
    }

    public void walk(AxiomSetContainers axiomSetContainter) 
    {
        for (AxiomSetContainers axiomSetSubContainer : axiomSetContainter.SubSetContainers) 
        {
            if (checkMaybeSkipped(axiomSetSubContainer)) 
            {
                continue;
            }
            
            checkConsistencyAndDrillDownIfNeeded(
                axiomSetSubContainer);
        }
    }

    protected void checkConsistencyAndDrillDownIfNeeded(
        AxiomSetContainers axiomSetSubContainer) 
    {
        if (checkIfConsistent(axiomSetSubContainer)) 
        {
            return;
        }
        
        if (!checkIfInconsistent(axiomSetSubContainer)) 
        {
            axiomSetSubContainer.ConsistencyType = 
                ConsistencyTypes.unknown;
            
            report(
                axiomSetSubContainer.toString(), 
                ConsistencyTypes.unknown);
        }
        
        walk(
            axiomSetSubContainer);
    }

    protected boolean checkIfConsistent(
        AxiomSetContainers axiomSetSubContainer)
    {
        ConsistencyTypes consistencyStatus;

        consistencyStatus = 
            checkByTool(
                axiomSetSubContainer,
                ToolForConsistencyConfiguration);

        if (consistencyStatus == ConsistencyTypes.consistent)
        {
            axiomSetSubContainer.setConsistentType();

            _consistentAxiomSetContaintersSet.add(
                axiomSetSubContainer);

            report(axiomSetSubContainer.toString(),
                consistencyStatus);

            return 
                true;
        }
        
        return 
            false;
    }
    
    protected boolean checkIfInconsistent(
        AxiomSetContainers axiomSetSubContainer)
    {
        ConsistencyTypes consistencyStatus;
        
        consistencyStatus = 
            checkByTool(
                axiomSetSubContainer,
                ToolForInconsistencyConfiguration);

        if (consistencyStatus == ConsistencyTypes.inconsistent)
        {
            axiomSetSubContainer.setInconsistentType();

            _inconsistentAxiomSetContaintersSet.add(
                axiomSetSubContainer);

            report(axiomSetSubContainer.toString(),
                consistencyStatus);

            return 
                true;
        }
        
        return 
            false;
    }
    

    protected void report(
        String axiomSetName, 
        ConsistencyTypes isSatisfiable) 
    {
        try 
        {
            _writer.write(
                axiomSetName + " = " + isSatisfiable + System.lineSeparator());
            
            _writer.flush();
        } 
        catch (IOException ex) 
        {
            ex.printStackTrace();
        }
    }

    protected boolean checkMaybeSkipped(
        AxiomSetContainers axiomSetContainter) 
    {
        if (axiomSetContainter.ConsistencyType != ConsistencyTypes.untested) 
        {
            return 
                true;
        }
        
        for (AxiomSetContainers consistentAxiomSetContainer : _consistentAxiomSetContaintersSet) 
        {
            if (consistentAxiomSetContainer.SubSetContainers.contains(axiomSetContainter)) 
            {
                axiomSetContainter.ConsistencyType = 
                    ConsistencyTypes.consistent;
                
                return 
                    true;
            }
        }
        
        for (AxiomSetContainers inconsistentAxiomSetContainer : _inconsistentAxiomSetContaintersSet) 
        {
            if (inconsistentAxiomSetContainer.SuperSetContainers.contains(axiomSetContainter)) 
            {
                axiomSetContainter.ConsistencyType = 
                    ConsistencyTypes.inconsistent;
                
                return 
                    true;
            }
        }
        return false;
    }
    
    protected abstract ConsistencyTypes checkByTool(
        AxiomSetContainers axiomSetSubContainer,
        String consistencyToolConfig);
}

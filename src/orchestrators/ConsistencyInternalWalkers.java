/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package orchestrators;

import consistencycheckers.ConsistencyCheckers;
import fileexecutors.UnixEFileExecutors;
import fileexecutors.UnixFileExecutors;
import fileexecutors.UnixProver9Mace4CommonFileExecutors;
import objects.AxiomSetContainers;
import objects.ConsistencyTypes;
import readers.*;
import writers.*;

/**
 *
 * @author pawelgarbacz
 */
public class ConsistencyInternalWalkers extends ConsistencyWalkers
{
    @Override
    protected ConsistencyTypes checkByTool(
        AxiomSetContainers axiomSetContainter,
        String toolConfiguration)
    {
        AxiomsWriters axiomsWriter = 
            new AxiomsProver9Writers(
                _constantAxiomSetText);
        
        AxiomsReaders axiomsReader = 
            new AxiomsProver9Readers();
        
        UnixFileExecutors unixFileExecutor = 
            new UnixProver9Mace4CommonFileExecutors(
                "prover9");
        
        switch (toolConfiguration)
        {
            case "prover9" :  
                axiomsWriter = 
                    new AxiomsProver9Writers(
                        _constantAxiomSetText);
        
                axiomsReader = 
                    new AxiomsProver9Readers();
        
                unixFileExecutor = 
                    new UnixProver9Mace4CommonFileExecutors(
                        "prover9");
                
                break;
                
            case "mace4" : 
                axiomsWriter = 
                    new AxiomsMace4Writers(
                        _constantAxiomSetText);
        
                axiomsReader = 
                    new AxiomsMace4Readers();
        
                unixFileExecutor = 
                    new UnixProver9Mace4CommonFileExecutors(
                        "mace4");
                
                break;
                
            case "eprover" :  
                axiomsWriter = 
                    new AxiomsEWriters(
                        _constantAxiomSetText);
        
                axiomsReader = 
                    new AxiomsEReaders();
        
                unixFileExecutor = 
                    new UnixEFileExecutors(
                        "eprover");
        }
        
        ConsistencyCheckers consistencyChecker = 
            new ConsistencyCheckers();
        
        ConsistencyTypes consistencyStatus = 
            consistencyChecker.check(axiomSetContainter, 
                axiomsReader, 
                axiomsWriter, 
                unixFileExecutor);
        
        return 
            consistencyStatus;
    }
}

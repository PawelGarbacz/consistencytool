/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package orchestrators;

import consistencycheckers.ConsistencyCheckers;
import fileexecutors.UnixRemoteTPTPFileExecutors;
import objects.AxiomSetContainers;
import objects.ConsistencyTypes;
import readers.*;
import writers.*;

/**
 *
 * @author pawelgarbacz
 */
public class ConsistencyTPTPWalkers extends ConsistencyWalkers
{
    @Override
    protected ConsistencyTypes checkByTool(
        AxiomSetContainers axiomSetContainter,
        String proverList)
    {
        AxiomsTPTPWriters axiomsTPTPWriter = 
            new AxiomsTPTPWriters(
                _constantAxiomSetText);
        
        AxiomsTPTPReaders axiomsTPTPReader = 
            new AxiomsTPTPReaders();
        
        UnixRemoteTPTPFileExecutors unixRemoteTPTPFileExecutor = 
            new UnixRemoteTPTPFileExecutors(
                proverList);
        
        ConsistencyCheckers consistencyChecker = 
            new ConsistencyCheckers();
        
        ConsistencyTypes consistencyStatus = 
            consistencyChecker.check(
                axiomSetContainter, 
                axiomsTPTPReader, 
                axiomsTPTPWriter, 
                unixRemoteTPTPFileExecutor);
        
        return 
            consistencyStatus;
    }
}

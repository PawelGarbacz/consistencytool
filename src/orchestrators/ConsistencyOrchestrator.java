/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package orchestrators;

import configurations.ConsistencyConfiguration;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.time.*;
import java.util.HashSet;
import java.util.TreeMap;
import objects.AxiomSetContainers;
import objects.CodingAxiomSetContainers;
import objects.Inheritors;
import readers.AxiomsTPTPReaders;

/**
 *
 * @author pawelgarbacz
 */
public class ConsistencyOrchestrator {

    private static Writer _resultsWriter;
    private static Writer _summaryWriter;
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) 
    {
        long start = 
            Clock.systemUTC().millis();
        
		String configFileName = args[0];
		
		if (configFileName == null)
			configFileName = "config.properties";
			
        ConsistencyConfiguration.configure(
			configFileName);
        
        setUpResultsWriter();
        
        setUpSummaryWriter();
        
        AxiomsTPTPReaders axiomsTPTPReader = 
            new AxiomsTPTPReaders();
        
        CodingAxiomSetContainers initialAxiomSetContainter = 
            axiomsTPTPReader.getFullAxiomSet(
                ConsistencyConfiguration.MainFolder + ConsistencyConfiguration.VariableAxiomsFileName);
        
        String constantAxiomSetText = 
            axiomsTPTPReader.readAxiomTextFromTextFile(
                ConsistencyConfiguration.MainFolder + ConsistencyConfiguration.ConstantAxiomsFileName);
        
        TreeMap<Integer, AxiomSetContainers> axiomSetPowersetCodedMap = 
            axiomsTPTPReader.getAxiomSetContainersPowerSetCodedMap(
                initialAxiomSetContainter);
        
        Inheritors inheritor = 
            new Inheritors(
                axiomSetPowersetCodedMap,
                initialAxiomSetContainter);
        
        inheritor.inherit();
        
        long prepEnd = 
            Clock.systemUTC().millis();
        
        try 
        {
           _resultsWriter.write(
               "Preparation took " + (prepEnd-start) + " miliseconds" + System.lineSeparator()); 
        } 
        catch (IOException ex) {ex.printStackTrace();}
        
        ConsistencyWalkers consistencyWalker = 
            ConsistencyConfiguration.ConsistencyWalker;
        
        consistencyWalker.setUp(
            axiomSetPowersetCodedMap, 
            initialAxiomSetContainter, 
            constantAxiomSetText, 
            _resultsWriter);
        
        consistencyWalker.walk(
            axiomSetPowersetCodedMap.get(
                axiomSetPowersetCodedMap.keySet().size()-1));
        
        summarise(
            axiomSetPowersetCodedMap);
        
        long end = 
            Clock.systemUTC().millis();
        
        try 
        {
           _resultsWriter.write(
               "Check took " + (end-prepEnd) + " miliseconds"); 
           
           _resultsWriter.flush();
           
           _resultsWriter.close();
        } 
        catch (IOException ex) {ex.printStackTrace();}
        }
    
    private static void setUpResultsWriter()
    {
        String resultsFileName = 
            ConsistencyConfiguration.MainFolder + ConsistencyConfiguration.ResultsFileName;
        
        try 
        {
            _resultsWriter = 
                new BufferedWriter(
                    new OutputStreamWriter(
                       new FileOutputStream(resultsFileName), 
                       "utf-8")); 
        } 
        catch (IOException ex) {ex.printStackTrace();}
    }
    
    private static void setUpSummaryWriter()
    {
        String resultsFileName = 
            ConsistencyConfiguration.MainFolder + ConsistencyConfiguration.SummaryFileName;
        
        try 
        {
            _summaryWriter = 
                new BufferedWriter(
                    new OutputStreamWriter(
                        new FileOutputStream(resultsFileName), 
                        "utf-8")); 
        } 
        catch (IOException ex) {ex.printStackTrace();}
    }
    
    private static void summarise(
        TreeMap<Integer, AxiomSetContainers> axiomSetPowersetCodedMap)
    {
        
        ConsistencySummarisers consistencySummariser = 
            new ConsistencySummarisers();
        
        consistencySummariser.get(
            new HashSet<>(
                axiomSetPowersetCodedMap.values()));
        try
        {
            for (AxiomSetContainers axiomSetContainer : consistencySummariser._maxConsistentAxiomSetsSet)
                _summaryWriter.write(
                    axiomSetContainer.toString() + " is max consistent " + System.lineSeparator());
            
            for (AxiomSetContainers axiomSetContainer : consistencySummariser._minInconsistentAxiomSetsSet)
                _summaryWriter.write(
                    axiomSetContainer.toString() + " is min inconsistent " + System.lineSeparator());
            
            _summaryWriter.flush();
            
            _summaryWriter.close();
        }
        catch (IOException e) {e.printStackTrace();}
    }
}

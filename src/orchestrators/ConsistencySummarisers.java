/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package orchestrators;

import java.util.HashSet;
import objects.AxiomSetContainers;
import objects.ConsistencyTypes;

/**
 *
 * @author pawelgarbacz
 */
public class ConsistencySummarisers 
{
    public HashSet<AxiomSetContainers> _minInconsistentAxiomSetsSet = 
        new HashSet<>();
    
    public HashSet<AxiomSetContainers> _maxConsistentAxiomSetsSet = 
        new HashSet<>();
        
    public  void get(
        HashSet<AxiomSetContainers> axiomSetPowerset)
    {
        for (AxiomSetContainers axiomSetContainer : axiomSetPowerset)
        {
            addToInconsistentSetIfNeeded(
                axiomSetContainer);
            
            addToConsistentSetIfNeeded(
                axiomSetContainer);
        }
    }
    
    private void addToInconsistentSetIfNeeded(
        AxiomSetContainers axiomSetContainer)
    {
        if (axiomSetContainer.ConsistencyType != ConsistencyTypes.inconsistent)
            return;
        
        if (_minInconsistentAxiomSetsSet.isEmpty())
        {
            _minInconsistentAxiomSetsSet.add(
                axiomSetContainer);
            
            return;
        }
        
        boolean axiomsNeedToBeAdded = true;
        
        boolean axiomsNeedToBeReplaced = false;
        
        HashSet<AxiomSetContainers> axiomSetContainersToBeReplacedSet = 
            new HashSet<>();
        
        for (AxiomSetContainers minInconsistentAxiomSetContainter : _minInconsistentAxiomSetsSet)
        {
            if (minInconsistentAxiomSetContainter.SubSetContainers.contains(axiomSetContainer))
            {
                axiomSetContainersToBeReplacedSet.add( 
                    minInconsistentAxiomSetContainter);
                
                axiomsNeedToBeReplaced = true;
            }
            
            if (axiomSetContainer.SubSetContainers.contains(minInconsistentAxiomSetContainter))
            {
                axiomsNeedToBeAdded = 
                    false;
            }
        }
        
        if (axiomsNeedToBeReplaced)
        {
            _minInconsistentAxiomSetsSet.removeAll(
                axiomSetContainersToBeReplacedSet);
            
            _minInconsistentAxiomSetsSet.add(
                axiomSetContainer);
            
            return;
        }
        
        if (axiomsNeedToBeAdded) 
            _minInconsistentAxiomSetsSet.add(
                axiomSetContainer);
    }
    
    private void addToConsistentSetIfNeeded(
        AxiomSetContainers axiomSetContainer)
    {
        if (axiomSetContainer.ConsistencyType != ConsistencyTypes.consistent)
            return;
        
        if (_maxConsistentAxiomSetsSet.isEmpty())
        {
            _maxConsistentAxiomSetsSet.add(
                axiomSetContainer);
            
            return;
        }
        
        boolean axiomsNeedToBeAdded = true;
        
        boolean axiomsNeedToBeReplaced = false;
        
        HashSet<AxiomSetContainers> axiomSetContainersToBeReplacedSet = 
            new HashSet<>();
        
        for (AxiomSetContainers maxConsistentAxiomSetContainter : _maxConsistentAxiomSetsSet)
        {
            if (maxConsistentAxiomSetContainter.SuperSetContainers.contains(axiomSetContainer))
            {
                axiomsNeedToBeReplaced = 
                    true;
                
                axiomSetContainersToBeReplacedSet.add(
                    maxConsistentAxiomSetContainter);
            }
            
            if (axiomSetContainer.SuperSetContainers.contains(maxConsistentAxiomSetContainter))
            {
                axiomsNeedToBeAdded = 
                    false;
            }
        }
        
        if (axiomsNeedToBeReplaced)
        {
            _maxConsistentAxiomSetsSet.removeAll(
                axiomSetContainersToBeReplacedSet);
            
            _maxConsistentAxiomSetsSet.add(
                axiomSetContainer);
            
            return;
        }
        
        if (axiomsNeedToBeAdded) 
            _maxConsistentAxiomSetsSet.add(
                axiomSetContainer);
    }
}

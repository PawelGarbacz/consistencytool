/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package configurations;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import orchestrators.ConsistencyInternalWalkers;
import orchestrators.ConsistencyTPTPWalkers;
import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.ex.ConfigurationException;

/**
 *
 * @author BORO Ontologist
 */
public class ConsistencyConfigurationReader 
{
	public static void read(String configFileName) throws ConfigurationException
	{
		PropertiesConfiguration config = 
			new PropertiesConfiguration();

		try (Reader reader = new BufferedReader(new InputStreamReader(
              new FileInputStream(configFileName), "utf-8"))) 
        {
            config.read(
				reader);
        } 
        catch (IOException ex) {ex.printStackTrace();}
		
		ConsistencyConfiguration.MainFolder = 
			(String) config.getProperty(
				"MainFolder");
		
		ConsistencyConfiguration.ToolForConsistencyConfiguration = 
			(String) config.getProperty(
				"ToolForConsistencyConfiguration");
		
		ConsistencyConfiguration.ToolForInconsistencyConfiguration = 
			(String) config.getProperty(
				"ToolForInconsistencyConfiguration");
		
		ConsistencyConfiguration.ConstantAxiomsFileName = 
			(String) config.getProperty(
				"ConstantAxiomsFileName");
		
		ConsistencyConfiguration.VariableAxiomsFileName = 
			(String) config.getProperty(
				"VariableAxiomsFileName");
		
		ConsistencyConfiguration.ResultsFileName = 
			(String) config.getProperty(
				"ResultsFileName");
		
		ConsistencyConfiguration.SummaryFileName = 
			(String) config.getProperty(
				"SummaryFileName");
		
		String consistencyWalkerType = 
			(String) config.getProperty(
				"ConsistencyWalker");
		
		switch (consistencyWalkerType)
		{
			case "ConsistencyTPTPWalkers" : 
				ConsistencyConfiguration.ConsistencyWalker = 
					new ConsistencyTPTPWalkers();
				
				break;
				
			case "ConsistencyInternalWalkers" : 
				ConsistencyConfiguration.ConsistencyWalker = 
					new ConsistencyInternalWalkers();
				
				break;	
		}
	}
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package configurations;

import orchestrators.ConsistencyWalkers;
import org.apache.commons.configuration2.ex.ConfigurationException;

/**
 *
 * @author pawelgarbacz
 */
public class ConsistencyConfiguration 
{
    public static String ToolForConsistencyConfiguration;
    public static String ToolForInconsistencyConfiguration;
    public static String MainFolder;
    public static ConsistencyWalkers ConsistencyWalker;
    public static String ConstantAxiomsFileName;
    public static String VariableAxiomsFileName;
    public static String ResultsFileName;
    public static String SummaryFileName;
    
    public static void configure(
		String configFileName)
	{
		try 
		{
			ConsistencyConfigurationReader.read(
				configFileName);
		}
		catch (ConfigurationException ex) {ex.printStackTrace();}
	}
}

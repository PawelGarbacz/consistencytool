/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package configurations;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.ex.ConfigurationException;

/**
 *
 * @author BORO Ontologist
 */
public class ConsistencyConfigurationWriter 
{
	public static String ConfigFileName;
	
	 public static void main(String[] args)
	 {
		ConfigFileName = 
			"config.properties";
		 
		try 
		{
			write();
		} 
		catch (ConfigurationException ex) {ex.printStackTrace();}
	 }
	
	private static void write() throws ConfigurationException
	{
		PropertiesConfiguration config = 
			new PropertiesConfiguration();

		config.addProperty(
			"MainFolder", 
			"data" + File.separator);
		
		config.addProperty(
			"ConsistencyWalker", 
			"ConsistencyTPTPWalkers");
		
		config.addProperty(
			"ToolForConsistencyConfiguration", 
			"Mace4---1109a Paradox---3.0");

		config.addProperty(
			"ToolForInconsistencyConfiguration", 
			"CVC4---FOF-1.5pre E---1.9.1");

		config.addProperty(
			"ConstantAxiomsFileName", 
			"Axioms_Const.in");
		
		config.addProperty(
			"VariableAxiomsFileName", 
			"Axioms_Const.in");
		
		config.addProperty(
			"ResultsFileName", 
			"Results.txt");
		
		config.addProperty(
			"SummaryFileName", 
			"Summary.txt");
				
		try (Writer writer = new BufferedWriter(new OutputStreamWriter(
              new FileOutputStream(ConfigFileName), "utf-8"))) 
        {
            config.write(
				writer);
        } 
        catch (IOException ex) {ex.printStackTrace();}
	}
}

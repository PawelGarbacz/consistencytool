/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fileexecutors;

/**
 *
 * @author pawelgarbacz
 */
public class UnixProver9Mace4CommonFileExecutors extends UnixFileExecutors
{
    public UnixProver9Mace4CommonFileExecutors(
        String toolName) 
    {
        super(
            toolName);
    }
    @Override
    protected void prepareShellContent()
    {
        _shellFileContent = 
            "data/" + _toolName + " -f " + _inputFileName + " > " + _outputFileName;
    }
}

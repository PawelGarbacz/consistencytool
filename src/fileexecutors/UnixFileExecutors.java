/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fileexecutors;

import configurations.ConsistencyConfiguration;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;

/**
 *
 * @author pawelgarbacz
 */
public abstract class UnixFileExecutors 
{
    protected String _toolName;
    protected String _shellFileName = 
        ConsistencyConfiguration.MainFolder + "run.sh";
    protected String _shellFileContent;
    protected String _inputFileName;
    protected String _outputFileName;
    
    
    public UnixFileExecutors(
        String toolName)
    {
        _toolName = 
            toolName;
    }
    
    public void writeShellFile(
        String fileName)
    {
        _inputFileName = 
            fileName;
        
        _outputFileName = 
            _inputFileName.replace(
                ".in", 
                ".out");
        
        prepareShellContent();
        
        try (Writer writer = new BufferedWriter(new OutputStreamWriter(
              new FileOutputStream(_shellFileName), "utf-8"))) 
        {
            writer.write(
                _shellFileContent);
        } 
        catch (IOException ex) {ex.printStackTrace();}
    }

    public String execCmd() 
    {
        String val = "";
     
        try 
        {
            Process permission = 
                Runtime.getRuntime().exec("chmod +x " + _shellFileName);
            
            try
            {
                permission.waitFor();
            } 
            catch (InterruptedException ex) {ex.printStackTrace();}
            
            Process proc = 
                Runtime.getRuntime().exec("./"+_shellFileName);
        
            try
            {
                proc.waitFor();
                
                proc.destroyForcibly();
            } 
            catch (InterruptedException ex) {ex.printStackTrace();}
        
        } catch (java.io.IOException ex) {ex.printStackTrace();}
        
        return 
            _outputFileName;
    }
    
    protected abstract void prepareShellContent();
}

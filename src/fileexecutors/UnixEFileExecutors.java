/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fileexecutors;



/**
 *
 * @author pawelgarbacz
 */
public class UnixEFileExecutors extends UnixFileExecutors
{
    public UnixEFileExecutors(
        String toolName) 
    {
        super(
            toolName);
    }
    
    @Override
    protected void prepareShellContent()
    {
        _shellFileContent = 
            "data/" + 
            _toolName + 
            " --auto-schedule --tptp3-in --soft-cpu-limit -m 6000 --proof-object=1 " + 
            _inputFileName + 
            " -o " + _outputFileName;
    }
}

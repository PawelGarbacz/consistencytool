/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fileexecutors;

import configurations.ConsistencyConfiguration;
import readers.PlainTextReaders;



/**
 *
 * @author pawelgarbacz
 */
public class UnixRemoteTPTPFileExecutors extends UnixFileExecutors
{
    private String _shellFileTemplate = 
        ConsistencyConfiguration.MainFolder + "ShellTemplate.txt";
    
    public UnixRemoteTPTPFileExecutors(
        String toolName) 
    {
        super(
            toolName);
    }
    
    @Override
    protected void prepareShellContent()
    {
        PlainTextReaders plainTextReader = 
            new PlainTextReaders();
        
        _shellFileContent = 
            plainTextReader.read(
                _shellFileTemplate); 
        
        _shellFileContent = 
            _shellFileContent.replace(
                "_toolName", 
                _toolName);
        
        _shellFileContent = 
            _shellFileContent.replace(
                "_inputFileName", 
                _inputFileName);
        
        _shellFileContent = 
            _shellFileContent.replace(
                "_outputFileName", 
                _outputFileName);
    }
}
